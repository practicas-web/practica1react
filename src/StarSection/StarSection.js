import React from 'react'
import Star from './Star'


function StarSection(props) {
    return(

        <div className="welcome-principal">

            <div className="welcome-star">
                <h1 className="titulo-star">10,000+ of our users love our products.</h1>
                <p className="sub-star">We only provide great products combined with excellent customer service. See what our satisfied customers
                    are saying about our services.</p>
            </div>

            <div className="text-star">
                <Star titleStar='Rated 5 Stars in Reviews'/>
                <Star titleStar='Rated 5 Stars in Report Guru'/>
                <Star titleStar='Rated 5 Stars in BestTech'/>
            </div>

        </div>
    )
}
export default StarSection