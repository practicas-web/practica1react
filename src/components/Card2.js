
import React from 'react'
import iconstarImg from '../images/icon-star.svg'
import './styles/Card.css'


class Card extends React.Component{
    render(){
        return(

            <div className="card">
                    <div className="card-principal">

                        <div className="card-one">
                            <img src={iconstarImg} className="img-star"/>
                            <p>{this.props.descriptionstar} </p>
                      
                        </div>

                    </div>
                
            </div>



            
        )
    }

}
export default Card