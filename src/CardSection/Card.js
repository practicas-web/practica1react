import React from 'react'
import primeraImg from '../images/image-anne.jpg'
import segundaImg from '../images/image-colton.jpg'
import terceraImg from '../images/image-irene.jpg'
import './Card.css'

function Card(props) {
    return(

        <div className="container-card">

            <div className="container-uno">
                 <img src={segundaImg} className="peruno"/>
                 <p className="name">Colton Smith</p>
                 <p className="name2"> Verified Buyer'</p>
                 <p className="texto">"We needed the same printed design...</p>
                 
            </div>
          
            <div className="container-dos">
                 <img src={terceraImg} className="perdos"/>
                 <p className="name">Irene Roberts</p>
                 <p className="name2"> Verified Buyer'</p>
                 <p className="texto"> "Customer service is always excellent...</p>
            </div>

            
            <div className="container-tres">
                 <img src={primeraImg} className="pertres"/>
                 <p className="name">Anne Wallace</p>
                 <p className="name2"> Verified Buyer'</p>
                 <p className="texto"> "Put an order with this company and can...</p>
            </div>

        </div>

        

        
    )
}
export default Card